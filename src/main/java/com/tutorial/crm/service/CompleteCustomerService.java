//package com.tutorial.crm.service;
//
//import com.tutorial.crm.bo.CustomerBo;
//import com.tutorial.crm.repository.CustomerRepository;
//import lombok.Setter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class CompleteCustomerService {
//  @Setter(onMethod = @__({@Autowired}))
//  private CustomerRepository customerRepository;
//
//  public List<CustomerBo> findByLastName(String lastName) {
//    return customerRepository.findbyLastname(lastName);
//  }
//}
