package com.tutorial.crm.service;

import com.tutorial.crm.converter.CustomerConverter;
import com.tutorial.crm.dto.CustomerDTO;
import com.tutorial.crm.entity.Customer;
import com.tutorial.crm.repository.CustomerRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

  @Setter(onMethod = @__({@Autowired}))
  private CustomerRepository customerRepository;

  @Setter(onMethod = @__({@Autowired}))
  private CustomerConverter customerConverter;

  public Customer create (Customer customer){return  customerRepository.save(customer);}

  public List<CustomerDTO> findAll(){
    List<Customer> customers = customerRepository.findAll(Sort.by(Sort.Direction.ASC, "firstName"));
    List<CustomerDTO>customerDTOS = customerConverter.EntityListToDtolist(customers);
    return customerDTOS;
  }

  public CustomerDTO findOneDetailed(Long id){return customerRepository.findOneDetailed(id);}

  public void deleteById(Long id){customerRepository.deleteById(id);}

  public CustomerDTO save(CustomerDTO customer){
    Customer entity;

    if(customer.getId() == null){
      entity = new Customer();
    }else{
      entity = customerRepository.getOne(customer.getId());
    }
    entity = customerConverter.updateEntityWithDto(entity, customer);
    return  customerConverter.entityToDTO(customerRepository.save(entity));
  }


}
