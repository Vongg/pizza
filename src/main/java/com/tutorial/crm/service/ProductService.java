package com.tutorial.crm.service;

import com.tutorial.crm.converter.ProductConverter;
import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.dto.ProductDTO;
import com.tutorial.crm.entity.Invoice;
import com.tutorial.crm.entity.Product;
import com.tutorial.crm.repository.ProductRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

  @Setter(onMethod = @__({@Autowired}))
  private ProductRepository productRepository;

  @Setter(onMethod = @__({@Autowired}))
  private ProductConverter productConverter;

  public Product create (Product product){return  productRepository.save(product);}

  public List<ProductDTO> findAll(){
    List<Product> products = productRepository.findAll(Sort.by(Sort.Direction.DESC, "productName"));
    List<ProductDTO>productDTOS = productConverter.EntityListToDtolist(products);
    return productDTOS;
  }

  public List<ProductDTO> findAllByInvoiceId(Long id){
    return productRepository.findAllByInvoiceId(id);
  }

  public ProductDTO findOneDetailed(Long id){return productRepository.findOneDetailed(id);}

  public void deleteById(Long id){
    productRepository.deleteById(id);}

  public ProductDTO save(ProductDTO product){
    Product entity;

    if(product.getId() == null){
      entity = new Product();
    }else{
      entity = productRepository.getOne(product.getId());
    }
    entity = productConverter.updateEntityWithDto(entity, product);
    return  productConverter.entityToDTO(productRepository.save(entity));
  }


}
