package com.tutorial.crm.service;

import com.tutorial.crm.converter.InvoiceConverter;
import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.entity.Invoice;
import com.tutorial.crm.repository.InvoiceRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvoiceService {

  @Setter(onMethod = @__({@Autowired}))
  private InvoiceRepository invoiceRepository;

  @Setter(onMethod = @__({@Autowired}))
  private InvoiceConverter invoiceConverter;

  public Invoice create (Invoice invoice){return  invoiceRepository.save(invoice);}

  public List<InvoiceDTO> findAll(){
    List<Invoice> invoices = invoiceRepository.findAll(Sort.by(Sort.Direction.DESC, "invoiceDate"));
    List<InvoiceDTO>invoiceDTOS = invoiceConverter.EntityListToDtolist(invoices);
    return invoiceDTOS;
  }

  public List<InvoiceDTO> findAllByCustomerId(Long id){
    return invoiceRepository.findAllByCustomerId(id);
  }

  public InvoiceDTO findOneDetailed(Long id){return invoiceRepository.findOneDetailed(id);}

  public void deleteById(Long id){
    invoiceRepository.deleteById(id);}

  public InvoiceDTO save(InvoiceDTO invoice){
    Invoice entity;

    if(invoice.getId() == null){
      entity = new Invoice();
    }else{
      entity = invoiceRepository.getOne(invoice.getId());
    }
    entity = invoiceConverter.updateEntityWithDto(entity, invoice);
    return  invoiceConverter.entityToDTO(invoiceRepository.save(entity));
  }
}


