package com.tutorial.crm.api;

import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.dto.ProductDTO;
import com.tutorial.crm.service.ProductService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/crm/products")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
    RequestMethod.POST, RequestMethod.PUT})
public class ProductController {
  @Setter(onMethod = @__({@Autowired}))
  private ProductService productService;

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  private List<ProductDTO> findAllByInvoiceId(@PathVariable("id") Long id) {
    return productService.findAllByInvoiceId(id);
  }

  @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
  private void deleteById(@PathVariable("id") Long id) {
    productService.deleteById(id);
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private ProductDTO save(@RequestBody ProductDTO product) {
    return productService.save(product);
  }

}
