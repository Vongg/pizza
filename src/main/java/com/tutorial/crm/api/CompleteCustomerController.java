//package com.tutorial.crm.api;
//
//import com.tutorial.crm.bo.CustomerBo;
//import com.tutorial.crm.service.CompleteCustomerService;
//import lombok.Setter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@RequestMapping(value = "/crm/complete")
//@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
//    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
//    RequestMethod.POST, RequestMethod.PUT})
//public class CompleteCustomerController {
//
//  @Setter(onMethod = @__({@Autowired}))
//  private CompleteCustomerService completeCustomerService;
//
//  @RequestMapping(value = "/findByLastName", method = RequestMethod.GET)
//  public List<CustomerBo> findByLastName(@RequestParam("lastName") String lastName) {
//    return completeCustomerService.findByLastName(lastName);
//  }
//}
