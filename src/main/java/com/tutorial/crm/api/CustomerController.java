package com.tutorial.crm.api;

import com.tutorial.crm.dto.CustomerDTO;
import com.tutorial.crm.entity.Customer;
import com.tutorial.crm.service.CustomerService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/crm/customers")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
    RequestMethod.POST, RequestMethod.PUT})

public class CustomerController {

  @Setter(onMethod = @__({@Autowired}))
  private CustomerService customerService;

  @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private List<CustomerDTO> findAll(){return customerService.findAll();}

  @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private CustomerDTO findOneDetailed(@PathVariable("id") Long id){return customerService.findOneDetailed(id);}

  @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
  private void deleteById(@PathVariable("id") Long id) {
    customerService.deleteById(id);
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private CustomerDTO save(@RequestBody CustomerDTO customer) {
    return customerService.save(customer);
  }

  @RequestMapping(value = "/create", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private Customer create(@RequestBody Customer customer) {
    return customerService.create(customer);
  }
}
