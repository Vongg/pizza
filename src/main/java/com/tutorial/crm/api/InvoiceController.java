package com.tutorial.crm.api;

import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.service.InvoiceService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/crm/invoices")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*", methods = {
    RequestMethod.GET, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.DELETE,
    RequestMethod.POST, RequestMethod.PUT})
public class InvoiceController {

  @Setter(onMethod = @__({@Autowired}))
  private InvoiceService invoiceService;

  @RequestMapping(value = "/{id}", method = RequestMethod.GET)
  private List<InvoiceDTO> findAllByCustomerId(@PathVariable("id") Long id) {
    return invoiceService.findAllByCustomerId(id);
  }

  @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
  private void deleteById(@PathVariable("id") Long id) {
    invoiceService.deleteById(id);
  }

  @RequestMapping(value = "/save", method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  private InvoiceDTO save(@RequestBody InvoiceDTO invoice) {
    return invoiceService.save(invoice);
  }


}
