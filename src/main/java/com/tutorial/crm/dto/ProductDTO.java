package com.tutorial.crm.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProductDTO {
  public ProductDTO(Long id, String productName){
    this.id = id;
    this.productName = productName;
  }

  private Long id;
  private String productName;
}
