package com.tutorial.crm.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class InvoiceDTO {
  public InvoiceDTO(Long id, Date invoiceDate){
    this.id = id;
    this.invoiceDate = invoiceDate;
  }

  private Long id;
  private Date invoiceDate;
}
