package com.tutorial.crm.converter;

import com.tutorial.crm.dto.ProductDTO;
import com.tutorial.crm.entity.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductConverter {
  public ProductDTO entityToDTO(Product in){
    return new ProductDTO(in.getId(), in.getProductName());
  }

  public List<ProductDTO> EntityListToDtolist(List<Product> ins) {
    return ins.stream().map(this::entityToDTO).collect(Collectors.toList());
  }

  public Product updateEntityWithDto(Product entity, ProductDTO dto){
    entity.setProductName(dto.getProductName());
    return entity;
  }
}
