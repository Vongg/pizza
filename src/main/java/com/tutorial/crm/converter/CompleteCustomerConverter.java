//package com.tutorial.crm.converter;
//
//import com.tutorial.crm.bo.CustomerBo;
//import com.tutorial.crm.bo.InvoiceBo;
//import com.tutorial.crm.dto.CompleteCustomerDTO;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Component
//public class CompleteCustomerConverter {
//  public List<CompleteCustomerDTO> boListToDtolist(List<CustomerBo> ins) {
//    return ins.stream().map(this::boToDto).collect(Collectors.toList());
//  }
//
//  public CompleteCustomerDTO boToDto(CustomerBo in) {
//    CompleteCustomerDTO out = new CompleteCustomerDTO();
//    out.setId(in.getId());
//    out.setFirstName(in.getFirstName());
//    out.setLastName(in.getLastName());
//    out.setEmail(in.getEmail());
//    out.setInvoices(in.getInvoices());
//    return out;
//  }
//
//  public PersonBo dtoToBo(CompletePersonDto in) {
//    PersonBo out = new PersonBo();
//    out.setId(in.getId());
//    out.setFirstName(in.getFirstName());
//    out.setLastName(in.getLastName());
//    out.setDob(in.getDob());
//
//    AddressBo address = new AddressBo();
//    address.setCity(in.getCity());
//    address.setStreet(in.getStreet());
//    out.setAddress(address);
//
//    out.setSkills(in.getSkills().stream().map(SkillBo::new).collect(Collectors.toList()));
//
//    return out;
//  }
//}
