package com.tutorial.crm.converter;

import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.entity.Invoice;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class InvoiceConverter {
  public InvoiceDTO entityToDTO(Invoice in){
    return new InvoiceDTO(in.getId(), in.getInvoiceDate());
  }

  public List<InvoiceDTO> EntityListToDtolist(List<Invoice> ins) {
    return ins.stream().map(this::entityToDTO).collect(Collectors.toList());
  }

  public Invoice updateEntityWithDto(Invoice entity, InvoiceDTO dto){
    entity.setInvoiceDate(dto.getInvoiceDate());
    return entity;
  }
}
