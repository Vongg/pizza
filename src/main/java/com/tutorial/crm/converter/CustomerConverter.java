package com.tutorial.crm.converter;

import com.tutorial.crm.dto.CustomerDTO;
import com.tutorial.crm.entity.Customer;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerConverter {
  public CustomerDTO entityToDTO(Customer in){
    return new CustomerDTO(in.getId(), in.getFirstName(), in.getLastName(), in.getEmail());
  }

  public List<CustomerDTO> EntityListToDtolist(List<Customer> ins) {
    return ins.stream().map(this::entityToDTO).collect(Collectors.toList());
  }

  public Customer updateEntityWithDto(Customer entity, CustomerDTO dto){
    entity.setFirstName(dto.getFirstName());
    entity.setLastName(dto.getLastName());
    entity.setEmail(dto.getEmail());
    return entity;
  }
}
