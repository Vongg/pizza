package com.tutorial.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.tutorial.crm"})
public class CRMApplication {

  public static void main(String[] args) {
    SpringApplication.run(CRMApplication.class, args);
  }

}
