package com.tutorial.crm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "product")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_ingredients")
public class Product {
  @Id
  @Column(name = "product_id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private  Long id;

  @Column(name = "product_name")
  private String productName;

  @ManyToMany
  @JoinTable(
      name = "invoice_product",
      joinColumns = @JoinColumn(name = "product_id"),
      inverseJoinColumns = @JoinColumn(name = "invoice_id")
  )
  private List<Invoice> invoices;

  public Product(String productName){
    this.productName = productName;
  }
}
