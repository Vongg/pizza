package com.tutorial.crm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "customer")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_ingredients")
public class Customer {
  @Id
  @Column(name = "customer_id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private  Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "email")
  private String email;

  @OneToMany(mappedBy = "customer")
  private List<Invoice> invoices;

  public Customer(String firstName, String lastName, String email){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }

}
