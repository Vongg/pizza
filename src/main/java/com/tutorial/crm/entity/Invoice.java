package com.tutorial.crm.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "invoice")
@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_ingredients")
public class Invoice {
  @Id
  @Column(name = "invoice_id")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  private  Long id;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @Column(name = "invoice_date")
  @Temporal(TemporalType.DATE)
  private Date invoiceDate;

  @ManyToMany
  @JoinTable(
      name = "invoice_product",
      joinColumns = @JoinColumn(name = "invoice_id"),
      inverseJoinColumns = @JoinColumn(name = "product_id")
  )
  private List<Product> products;

  public Invoice(Date invoiceDate){
    this.invoiceDate = invoiceDate;
  }

}
