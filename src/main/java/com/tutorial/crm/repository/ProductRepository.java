package com.tutorial.crm.repository;

import com.tutorial.crm.dto.ProductDTO;
import com.tutorial.crm.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

  @Query(
      "SELECT new com.tutorial.crm.dto.ProductDTO(i.id, " +
          "i.productName)" +
          "FROM Product i " +
          "ORDER BY i.productName ASC"
  )
  List<ProductDTO> findAllByInvoiceId(@Param("id") Long invoiceId);

  @Query("SELECT new com.tutorial.crm.dto.ProductDTO(i.id, "
      + " i.productName) "
      + " FROM Product i "
      + " WHERE i.id = :id ")
  ProductDTO findOneDetailed(@Param("id") Long id);
}
