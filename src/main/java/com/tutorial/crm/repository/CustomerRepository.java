package com.tutorial.crm.repository;

import com.tutorial.crm.dto.CustomerDTO;
import com.tutorial.crm.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

//  @Query(
//      "SELECT new com.tutorial.crm.dto.CustomerDTO(i.id, " +
//          "i.firstName, " +
//          "i.lastName, " +
//          "i.email)" +
//          "FROM Customer i" +
//          "ORDER BY i.lastName ASC"
//  )
//
//  List<CustomerDTO> find;

  @Query("SELECT new com.tutorial.crm.dto.CustomerDTO(i.id, "
      + " i.firstName, " +
      "i.lastName, " +
      "i.email) "
      + " FROM Customer i "
      + " WHERE i.id = :id ")
  CustomerDTO findOneDetailed(@Param("id") Long id);

//  @Query(
//      "SELECT new com.tutorial.crm.bo.CustomerBo(p.id, p.firstName, p.lastName, p.email, p.invoices) "
//          + " FROM CustomerBo p "
//          + " WHERE lower(p.lastName) = lower(:lastName) "
//          + " ORDER BY p.id ASC ")
//  List<CustomerBo> findbyLastname(@Param("lastName") String lastName);
}
