package com.tutorial.crm.repository;

import com.tutorial.crm.dto.InvoiceDTO;
import com.tutorial.crm.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {


    @Query(
        "SELECT new com.tutorial.crm.dto.InvoiceDTO(i.id, " +
            "i.invoiceDate)" +
            "FROM Invoice i " +
            "ORDER BY i.invoiceDate DESC"
    )
    List<InvoiceDTO> findAllByCustomerId(@Param("id") Long customerId);

  @Query("SELECT new com.tutorial.crm.dto.InvoiceDTO(i.id, "
      + " i.invoiceDate) "
      + " FROM Invoice i "
      + " WHERE i.id = :id ")
  InvoiceDTO findOneDetailed(@Param("id") Long id);
}
