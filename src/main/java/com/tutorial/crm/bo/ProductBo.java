//package com.tutorial.crm.bo;
//
//
//import com.tutorial.crm.entity.Invoice;
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//import java.util.List;
//
//@Getter
//@Setter
//@Entity
//@Table(name = "product")
//@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_persons")
//public class ProductBo {
//
//  @Id
//  @Column(name = "id")
//  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
//  private Long id;
//
//  @Column(name = "product_name")
//  private String productName;
//
//  @ManyToMany
//  @JoinTable(
//      name = "invoice_product",
//      joinColumns = @JoinColumn(name = "product_id"),
//      inverseJoinColumns = @JoinColumn(name = "invoice_id")
//  )
//  private List<Invoice> invoices;
//
//}