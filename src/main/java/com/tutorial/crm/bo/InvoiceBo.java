//package com.tutorial.crm.bo;
//
//import com.tutorial.crm.entity.Product;
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.List;
//
//@Getter
//@Setter
//@Entity
//@Table(name = "invoice")
//@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_persons")
//public class InvoiceBo {
//
//  @Id
//  @Column(name = "id")
//  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
//  private Long id;
//
//  @Column(name = "invoice_date")
//  @Temporal(TemporalType.DATE)
//  private Date invoiceDate;
//
//  @ManyToMany
//  @JoinTable(
//      name = "invoice_product",
//      joinColumns = @JoinColumn(name = "invoice_id"),
//      inverseJoinColumns = @JoinColumn(name = "product_id")
//  )
//  private List<Product> products;
//}