//package com.tutorial.crm.bo;
//
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.*;
//import java.util.List;
//
//
//@Getter
//@Setter
//@Entity
//@Table(name = "customer")
//@SequenceGenerator(allocationSize = 1, name = "SEQ", sequenceName = "seq_customers")
//public class CustomerBo {
//
//  @Id
//  @Column(name = "id")
//  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
//  private Long id;
//
//  @Column(name = "first_name")
//  private String firstName;
//
//  @Column(name = "last_name")
//  private String lastName;
//
//  @Column(name = "email")
//  private String email;
//
//  @OneToMany(fetch = FetchType.LAZY)
//  @JoinColumn(name = "invoice_id")
//  private List<InvoiceBo> invoices;
//}